# Discussing workplace stress

## Flashcard

## Vocabulary I
You can talk about workplace health issues using noun forms
- She suffers from anxiety
- He's suffering from exhaustion
- She's suffering from depression
- I have a lot of stress in my life.

Or you can talk about the issues using adjective forms
- He's so overworked
- She always seems so anxious
- I'm so tired - absoluty exhausted
- I think she's depressed
- I'm under pressure with this deadline

## Vocabulary II
Use prhases like these to describe things that can hekp alleviate stress at work
- push back the deadline
- request more resources
- ask for some help 
- talk to someone about it
- take some time off
- take a vacation
- reduce your work hours
- redistribute the workload
- adjust the project plan 

## Expressions
Here are some questions you can ask if you want to find out what's already been done about an existing problem
- What have you tired so far?
- Have yoy spoken anyone about it?
- Did you ask for more resources

Here are question you can ask if you are not sure what to do 
- Any idea what to do?
- What are you going to do about it?
