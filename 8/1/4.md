# Writing about a song
You are going to give your opinions about a song on social media

## FlashCards
- edgy: nervous, not calm, unusual in a way fashionable or exciting
- chilled: cold, relaxing
- tunes: songs
- pace: (rhythm) the speed at which someone or something moves, or with which something happends or changes
- rubbish: waster material or things that are no longer wanted or needed
- shiver: When people or animal shiver, they shake slightly because they feel cold, ill, or frightened
- frightened:feeling fear or worry
- 

## Vocabulary

### Adjective for music
You may have strong opinions about music. Here are some common adjectives used for describing music.
- My mother loves listening to this soft, sentimental music.
- I like listening to anything romantic
- That song is so annoying
- Some people find this song offensive. The lyrics are a bit rude.
- I like music that's different. You know, a little edge.
- These chilled-out rhythms are great for relaxing
- I love dacing to tunes with an uptempo, funky beat.
- I love this band, their sound is so lo-fi

### Listening
- What are you think? A bit sentimental? 
- Too romantic
- No, I loved it, I think you have an amazing range.
- I like the change in pace and mood, faster, then slow, happy then sad. What did you think Sally?  
- There wasn't much of a melody
- Not much of a melody? That's not much of a melody?
- And it has kind of an annonying beat

## Expressions

### Describe a song
You can describe a song in several ways. You can talk about the singer's voice
- She can hit some really high notes
- The singer doesn't have much of a range
- He's got a great voice. It makes me shiver

You can talk about the song's rhythm
- You can really dance to it
- There isn't much of a beat

You can talk about the song's lyrics
- I love this song! The lyrics are so romantic!
- Ugh! Those lyrics are really offensive

And you can talk about the feeling, or meed of a song
- It makes me feel relaxed
- I like how the mood changes. First it's edgy then it's romantic

Use not much of a to say something is inadequate. Use kind of to say 'slightly'
- There isn't much of a beat
- The melody is kind of sentimental

## Writing
- I Just heard the new song Sent Sentimentally by Rough Stones
- The song has a lo-fi sound with a totally chatchy tune. The singer, Mike Groan, hits some really low notes.
- They make the song interesting and edgy. But be careful: the lyrics are kind of offensive.
- There's not much of a change in mood. It's all very chilled-out. But that's why I like it. It has a relaxed but uptempo feeling.
- What a great new song by the Rough Stones! Buy it now!

## Final Task
