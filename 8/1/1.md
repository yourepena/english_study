# Talking about music you like
You're going to discuss the kinds of that music you like now and liked in the past

## FlashCards

## Vocabulary
- You know, Ian says I know nothing about music but, you know, I tend to disagree, because I listen to almost any style of music
- I mean, I dont' listen to hip-hop or techno 
- I mean...I like music, I listen to it almost every day
- You know, I often listen to Beethoven, If I'm sad or moody ....If I feel like I need to relax, I'll put on some soft pop music, like Celine Dion.
- I mean, I agree, It's nothing special or terribly original, but you have to admit she has a great voice
- She sings beautifully

## Grammar

### Past preferences and habist
To talk about something you liked in the past but don't like now, use **used to**
- Kayla used to like punk music
- Ted used to like punk, but now he listens to hip-hop

To ask a question or make a negative statement with used to, drop the d.
- Did you use to like folk music?
- Folk music? Ummm, no, I didn't use to like folk music

You can use a negative question if you think you already know about someone's past preferences or habist
- Didn't you use to like techno?
- Yeah, I used to, but not anymore

Use still to describe preferences or habits that haven't changed
- Did you use to like punk?
- Yeah, and I **still** do.


## Final Task
- Hi. What are you listening to?
- Some great hip-hop. I got it from a DJ at a Club last night.
- Really? You used to listen to techno.
- Not anymore, Hip-Hop's what I'm into these days 
- But tyou used to hate Hip-Hop
- Yeas, and You used to be into cartoon, we all change
- I guess you're right, but it's too bad.
- What's too bad?
- I've got tickes for a techno club, but you don't like it
- Do you mean the new techno club on Green Street? Cool! Did I say I used to like techno? I still like techno!
- Are you sure you want to go?
- Of course, I do
- Great! I  ll call you later in the week
