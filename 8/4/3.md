# Arranging a meeting

## Flash cards

## Expressions 1
Use these expressions to express urgency:
- **It's crucial** that we find a solution
- **It's urgent** the company find a buyer
- **It's essencial** that he attend the meeting.
- **It's vital** no one discuss this information
- **It's critical** that we meet the deadline
- **It's imperative** all staff undestrand the situation

The structure adjective **+ that**, when it refers to the future, is generally followed by the base form of the verb.
- It's essential that he attend the meeting
- It's vital she report to us immediately

You can use the expression as soon as possible to stress that something needs to be done urgently
- We need to meet **as soon as possible**

Use the advern absoluty to increase the sene of urgency:
- It's **absolutly** critical that we meet the deadline.
- It's **absolutely** crucial we find a solution

## Listening
- Look, It's absolutely critical that we meet. I want to ask you some questions about......It's already behind deadline
- Per, It's vital that we sit down together and talk. We're paying your company a lot of money, It's essential we get an undestanding of what's going on.
- Look, We need to meet as soon as possible. WHen are free today? 


## Expressions 2
There many ways you can ask about someone's availability, alongside prhases you probably know such as **When are you free?**
- When are you available for a meeting?
- What day would suit you for for a meeting?
- Would Tuesday or Thursday work for you? 
- Can you do noon on Wednesday?

If someone asks about you availability, here are some ways to respond.
- Let me check my canlendar
- That day doesn't work for me.
- I'm sorry. I'm not available on Thursday
- I'll be there
- I look forward to seeing you.


