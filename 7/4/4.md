# Vocabulary
- amuse: to entertain someone, especially by humorous speech or action or by making them laugh or smile
- teach: taught

# Grammar
- Why do you like that show so much?
- I like it because the plot's complex and exciting.
- 'Fallen Love' wasn't shown on TV because of the violence.
- I watch this show with my kids because of the PG rating.

# Expressions
- I was surprised at how complex the rating system is. For example, there are more than 35 possible ratings!
- I my opinion, there's too much violence on TV. In 'Haet Wave' last night, three people were murdered in five minutes.
- Brent feels strongly that TV should be educational, not just entertaining, becaouse children are watching.

# Listening
- So, Hellen and I watch that Tv program you like.
- Miami Confidential?
- Yeah, there's a lot of violence, seven murder and that was just in last night's episode.
- I'ts pretty violent
- I just glad the kids were asleep.
- I'm amezd `at` how violent Tv show are these days.
- Do you use rating system?
- Yeah, but is so Complex, All the different rating - TV-G, TV-PG, TV-MA.
- It's crazy. Sometime is difficult to just find something educational.
- My kind is only interested in entretienment. You know?, I read an article,that the average american kid watches 20 thounsand advertisements a year.
- That is a lot of ads.
- I feel ~~very~~ `pretty` strongly that there should be some kind of restrincions.
- Oh, I agree, but listening to us, ~~been~~ `being` so nagative about TV when we ~~seat~~ `sit` in front `of it` every night.
- What happen`ed` in Miami Confidential last night?
- Ohh `it` was really good, So Bob Boulder had to catch this killer who had been on the run for 25 years.

# Final Task 
I really dont' like watch TV, because It never has anything interesting. I usually watch a lot of sitcom videos on youtube, and I also watch chess game videos and people talking about Programming. 
