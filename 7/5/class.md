
primary school



where
what
why
advantages
level
studied abroad
still interested in education




MBa = 1 year

Master of Arts = 1 year and a half

Master of Science (M.Sc.) = 2 years





Bachelor - 3 years

MSc - 2 years

------ Teaching in Aussie ---------

Ph.D. 





British Ph.D. - 3 years

American Ph.D. - 5 years



Target Language



• Answering questions about education: I went to Oxford. / I studied at Yale. / I was at the Sorbonne. / I studied medicine. / I did biology. / I did an MA in history.

• Asking questions about education: What degree did you do? / And where did you go to school? / Are you interested in any further education?




3 years: from the start to the moment of SUBMISSION



examination / defense / viva

corrections ( 1 - 12 months)

re-examination



September 2015 - START

September 2018 - SUBMISSION

February 2019 - VIVA (passed with corrections)

6-12 months

September 2019 - RESUBMISSION WITH CORRECTIONS

20th January 2020 - AWARD OF MY PH.D.



2 months ---> 5 months

2 months ---> 5 months



parchment





ready-to-go book



Academic Journals



peer-to-peer Journals


TOEFL / IELTS
TOEFL = American English
IELTS = British English
/120
80/120
85/120
90/120
100/120 (Harvard, YALE, ... )
Reading
Speaking
Listening
Writing
/30
90/120
22/30
IELTS
9.0
7.0 / 9.0




when / where did you get your degree?
what degree did you get?
what did you study?
have you ever studied abroad?
are you interested in further education?
Did you enjoy your studies?






Bachelor (BA)

Master (MSc, MA, MBA)

Doctorate (Ph.D.)





at the beginning, I was interested in getting/taking a master



I enjoyed my studies



I have never studied abroad





Bachelor - 2012

Master - 2014

Ph.D. - 2019



Arabic & Hebrew Language & Literature

Religious Studies (Koran & Old Testament)



Which country did you like the most?
