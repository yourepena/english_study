"the next week" ---> NEXT WEEK


"in monday 10 hours" ---> on Monday at 10


to arrange = to organize





CORRECTIONS

"the next week" ---> NEXT WEEK


"in monday 10 hours" ---> on Monday at 10 (am/pm)


at 10 in the morning / at 10 in the night





Target Language (part 1)



Set phrases for making appointments: I’d like to schedule an appointment with___________. / Some time next week? / I can’t do _____________ . / __________ works for me.



Words related to job interviews: schedule / Human Resources / appointment



PART 2

education
work experience
strengths vs. weaknesses
why are you interested in this position
career goals
expected salary
why do you want to leave your current job?


I have got a [Bachelor] in computer science from the University of...





2017

5-6

2020



Upon getting my degree, I kept working for the same company that I was working during my studies. Overall, I've been working there for 5-6 years





upon = from the moment that...





I'm bored in my job ---> I'm looking for something more challenging / more exciting



FEEDBACK

didn't 
looking forward to + gerund (ing) 
If I will be honest  -> If I'm being honest / honestly
responsible FOR a team of 5 people
I was responsible for designing something like Google Drive




INTERVIEW TOPICS

education
previous positions
experience
career goals
availability to move
strengths/ weaknesses
