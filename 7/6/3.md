# Dealing with confidential information

## Flash Card 
- show up: to arrive 
- gathering: party 
- plead: to make an urgent, emotional statement or request for something
- deflet: to prevent something 

## Grammar
Reflexive pronouns

### Referring backto the subject
Use reflexive pronouns to refer back to the subject of a sentence or clause. Reflexive pronouns are formed by adding `-self` to singular  pronouns or `-selves` to a plural pronoun.

#### Use reflexive pronouns when the subject and the object are the same:
- I promised myself I'd quit smoking
- He bougth himself a new suit
- You'll have to go yourselves. I can't come with you.

#### You can use reflexive pronouns as the object of a preposition
- I didn't buy it for you. I bougth it for myself.
- I didn't break it, honest. It stopped working by itself.

#### Singular

| Pronoun  | Reflexive Pronoun |
| ------ | ------ |
| I  | myself |
| you  | yourself |
| She | herself |
| He  | himself |
| It | itself |
| You | youselves |
| They  | themselves |
| We  | ourselves |

## Expressions
Now we're going to look at how to discuss confidential information
### Discussing confidential information 
If you want to tell someone that information is confidential, use expressions like these:
- I'm not suposed to tell anyone.
- Nobody knows about it except the boss.
- This is confidential information. 

If you share confidential information with someone, but want them to keep it secret, use expressions like these:
- Don't tell anybody about this.
- Keep this to yourself, would you?
- You can' tell anyone, ok?
- Keep this between you and me.

If someone refuses to tell you something, you can either let it go or plead for more information.
- I'm not supposed to tell anyone.
- Ok, Then don't tell me.
- The information's confidential.
- But you can tell me, rigth? Come on.

You may even want to deflect questions about sensitive information
- Never mind, It's nothing to worry about

## Final Task
- Oh never, mind. It's nothing to worry about.
- I'm sorry, but I'm not supposed tell anyone.
- All right. But keep this to yourself, would you?
- The company' opening a new office
- In London, But no one knows yet, Ok?
- It's confidential information. You can't tell anyone.
- The boss is going to announce it next month
