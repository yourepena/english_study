
# To be
- Gerund: **being**
- Past Participle: **been**

| Pronoun  | Simple Present | Simple Past |
| ------ | ------ | ------ |
| I | am | was |
| you | are | were |
| She/He/It | is | was |
| They | are | were |
| We | are | were |


# To have

- Gerund: **having**
- Past Participle: **had**

| Pronoun  | Simple Present | Simple Past |
| ------ | ------ | ------ |
| I | have | had |
| you | have | had |
| She/He/It | has | had |
| They | have | had |
| We | have | had |

# To do
- Gerund: **doing**
- Past Participle: **done**

| Pronoun  | Simple Present | Simple Past |
| ------ | ------ | ------ |
| I | do | did |
| you | do | did |
| She/He/It | does | did |
| They | do | did |
| We | do | did |


# To go
- Gerund: *going*
- Past Participle: *gone*

| Pronoun  | Simple Present | Simple Past |
| ------ | ------ | ------ |
| I | go | went |
| you | goes | went |
| She/He/It | go | went |
| They | go | went |
| We | go | went |

# To buy
- Gerund: *buying*
- Past Participle: *bought*

| Pronoun  | Simple Present | Simple Past |
| ------ | ------ | ------ |
| I | buy | bought |
| you | buys | bought |
| She/He/It | buy | bought |
| They | buy | bought |
| We | buy | bought |

# To teach 
- Gerund: *teaching*
- Past Participle: *taught*

| Pronoun  | Simple Present | Simple Past |
| ------ | ------ | ------ |
| I | teach | taught |
| you | teaches | taught |
| She/He/It | teach | taught |
| They | teach | taught |
| We | teach | taught |
