# Discussinf work-life balance

## Vocabulary

Use these expressions with off to talk about time off and vacations:
- Are you taking any time off?
- Yes. I have a week off in May  

- Brent get New Year's Day off.
- She's off next week  

Pay and hours
- I'm working overtime
- The compensation's good.  

Time away from work
- I get two weeks off
- I'm taking time off
- I'm off on Friday
- Takw tomorrow off  

Problems
- Something came up
- It's a computer issue.  


## Grammar

Sense verbs with as if and as though
- Frank feels as if there aren't enough hours in the day.
- Oscar feels as though there aren't enough in the night.  

Use a sense verb **as if** or **as though** to talk about your perception of a situation.
- I fell as if I don't have enough time off.
- It looks as though I'll be compensated well.
- It sounds as if you worked a lot of overtime.  

In informal conversation, you will hear **like** used instead of **as if** or **as though**. This is technically incorrect but very common.
- It seems like he's been off for a month
- It looks like we'll have to work on the weekend. 

## Expressions

- Do you ever have any doubts?
- No - none at all
- I'm completely sure
- I'm wondering if
- I doubt it.
- You're sure?
- absolutely

## Final Task
- Work is so stressful right now, I feel like I live at the office.
- It sounds as though you've been working a lot of overtime
- Me tto. Somethin came up again this week, so I've been working every night.
- Yeah. Sometimes I wonder about my career choice. 
- Do you ever have any doubts about your career choice?
- Maybe we just need to take some time off.
- I'm off this summer for 10 days.
- But I wish I could take time off right now. Summer feel like years away.
